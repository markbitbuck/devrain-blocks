<?php

namespace DevRain\Blocks\BlockTypes;


/**
 * AbstractDynamicBlock class.
 */
abstract class AbstractDynamicBlock {

	/**
	 * Block namespace.
	 *
	 * @var string
	 */
	protected $namespace = 'devrain';

	/**
	 * Block namespace.
	 *
	 * @var string
	 */
	protected $block_name = '';

	/**
	 * Registers the block type with WordPress.
	 */
	public function register_block_type() {
		register_block_type(
			$this->namespace . '/' . $this->block_name,
			array(
				'render_callback' => array( $this, 'render' ),
				'editor_script'   => 'dr-' . $this->block_name,
				'editor_style'    => 'dr-block-editor',
				'style'           => 'dr-block-style',
				'attributes'      => $this->get_attributes(),
				'script'          => 'dr-frontend',
			)
		);
	}

	/**
	 * Include and render a dynamic block.
	 *
	 * @param array  $attributes Block attributes. Default empty array.
	 * @param string $content    Block content. Default empty string.
	 * @return string Rendered block type output.
	 */
	abstract public function render( $attributes = array(), $content = '' );

	/**
	 * Get block attributes.
	 *
	 * @return array
	 */
	protected function get_attributes() {
		return array();
	}
}
