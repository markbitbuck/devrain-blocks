<?php

namespace DevRain\Blocks\BlockTypes;

/**
 * Slider class.
 */
class Slider extends AbstractDynamicBlock
{
	/**
	 * Block name.
	 *
	 * @var string
	 */
	protected $block_name = 'slider';

	/**
	 * @inheritDoc
	 */
	public function render($attributes = array(), $content = '')
	{
		$data = rawurlencode( wp_json_encode($attributes));

		wp_add_inline_script(
			'dr-frontend',
			"var sliderSettings = sliderSettings || JSON.parse( decodeURIComponent( '$data' ) );",
			'before'
		);

		return "<div class=\"dv-block-slider\"></div>";
	}
}
