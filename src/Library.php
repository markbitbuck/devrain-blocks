<?php

namespace DevRain\Blocks;

/**
 * Library class.
 */
class Library {

	/**
	 * Initialize block library features.
	 */
	public function init() {
		add_action( 'init', [ $this, 'register_blocks' ]);
	}

	/**
	 * Register blocks, hooking up assets and render functions as needed.
	 */
	public function register_blocks() {
		$blocks = [
			'Slider',
		];
		foreach ( $blocks as $class ) {
			$class    = __NAMESPACE__ . '\\BlockTypes\\' . $class;
			$instance = new $class();
			$instance->register_block_type();
		}
	}
}
