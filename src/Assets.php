<?php

namespace DevRain\Blocks;


/**
 * Assets class.
 */
class Assets {

	/**
	 * Initialize class features on init.
	 */
	public function init() {
		add_action( 'init', [ $this, 'register_assets' ] );
	}

	/**
	 * Register block scripts & styles.
	 */
	public function register_assets()
	{
		$this->register_style( 'dr-block-editor', plugins_url( 'build/editor.css', __DIR__ ), array( 'wp-edit-blocks' ) );
		$this->register_style( 'dr-block-style', plugins_url( 'build/style.css', __DIR__ ), array( 'wp-components' ) );

		// Shared libraries and components across all blocks.
		$this->register_script( 'dr-blocks', plugins_url( 'build/blocks.js', __DIR__ ), array() );
		$this->register_script( 'dr-vendors', plugins_url( 'build/vendors.js', __DIR__ ), array() );
		//$this->register_script( 'dr-packages', plugins_url( 'build/packages.js', __DIR__ ), array(), false );
		$this->register_script( 'dr-frontend', plugins_url( 'build/frontend.js', __DIR__ ), array( 'dr-vendors' ) );

		// Individual blocks.
		$this->register_script( 'dr-slider', plugins_url( 'build/slider.js', __DIR__ ), array( 'dr-vendors', 'dr-blocks' ) );
	}


	/**
	 * Get the file modified time as a cache buster if we're in dev mode.
	 *
	 * @param string $file Local path to the file.
	 * @return string The cache buster value to use for the given file.
	 */
	protected function get_file_version( $file ) {
		if ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) {
			$file = trim( $file, '/' );
			return filemtime( DR_ABSPATH . $file );
		}
		return DR_VERSION;
	}

	/**
	 * Registers a script according to `wp_register_script`, additionally loading the translations for the file.
	 *
	 * @param string $handle    Name of the script. Should be unique.
	 * @param string $src       Full URL of the script, or path of the script relative to the WordPress root directory.
	 * @param array  $deps      Optional. An array of registered script handles this script depends on. Default empty array.
	 * @param bool   $has_i18n  Optional. Whether to add a script translation call to this file. Default 'true'.
	 */
	protected function register_script( $handle, $src, $deps = array() ) {
		$filename     = str_replace( plugins_url( '/', __DIR__ ), '', $src );
		$ver          = $this->get_file_version( $filename );
		$deps_path    = dirname( __DIR__ ) . '/' . str_replace( '.js', '.deps.json', $filename );
		$dependencies = file_exists( $deps_path ) ? json_decode( file_get_contents( $deps_path ) ) : array(); // phpcs:ignore WordPress.WP.AlternativeFunctions
		$dependencies = array_merge( $dependencies, $deps );

		wp_register_script( $handle, $src, $dependencies, $ver, true );
	}

	/**
	 * Registers a style according to `wp_register_style`.
	 *
	 * @param string $handle Name of the stylesheet. Should be unique.
	 * @param string $src    Full URL of the stylesheet, or path of the stylesheet relative to the WordPress root directory.
	 * @param array  $deps   Optional. An array of registered stylesheet handles this stylesheet depends on. Default empty array.
	 * @param string $media  Optional. The media for which this stylesheet has been defined. Default 'all'. Accepts media types like
	 *                       'all', 'print' and 'screen', or media queries like '(orientation: portrait)' and '(max-width: 640px)'.
	 */
	protected function register_style( $handle, $src, $deps = array(), $media = 'all' ) {
		$filename = str_replace( plugins_url( '/', __DIR__ ), '', $src );
		$ver      = $this->get_file_version( $filename );
		wp_register_style( $handle, $src, $deps, $ver, $media );
	}
}
