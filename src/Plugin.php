<?php

namespace DevRain\Blocks;


/**
 * Main plugin class.
 */
class Plugin {

	/**
	 * Version.
	 *
	 * @var string
	 */
	const VERSION = '0.0.0-dev';

	/**
	 * @var self
	 */
	protected static $instance;

	public static function init() {

		if(is_null(self::$instance)) {
			self::$instance = new static();
		}

		self::$instance->define_constants();

		(new Library())->init();
		(new Assets())->init();
	}

	/**
	 * Define plugin constants.
	 */
	protected function define_constants() {
		define( 'DR_VERSION', self::VERSION );
		define( 'DR_ABSPATH', $this->get_path() . '/' );
	}

	/**
	 * Return the path to the package.
	 *
	 * @return string
	 */
	public function get_path() {
		return dirname( __DIR__ );
	}
}
