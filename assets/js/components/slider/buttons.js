import {withFocusOutside, TextControl, Dashicon, IconButton} from '@wordpress/components';
import {RichText, URLInput} from "@wordpress/editor";
import {__} from "@wordpress/i18n";
import classnames from "classnames";

class Buttons extends React.Component {

	constructor() {
		super(...arguments);

		this.state = {
			buttonIndex: null,
		};
	}

	handleFocusOutside() {
		this.setState({buttonIndex: null})
	}

	render() {

		const {attributes, setAttributes, index} = this.props;

		const { buttons } = attributes.content[index];

		const entries = Object.keys(buttons);

		return (
			<div>
				<div className={`buttons`}>
					{
						entries.map((type, key) => {

							return (
								<span
									key={key}
									role="button"
									onClick={() => this.setState({buttonIndex: type})}
									onKeyPress={(event) => event.stopPropagation()}
									tabIndex={type}
									className="wp-block-button is-style-squared"
								>
									<RichText
										tagName="span"
										placeholder={__('Add text…')}
										value={buttons[type].text ? buttons[type].text : ''}
										onChange={(value) => {
											const content = [...attributes.content];
											content[index].buttons[type].text = value;
											setAttributes({content});
										}}
										formattingControls={['bold', 'italic', 'strikethrough']}
										className={classnames('wp-block-button__link')}
										keepPlaceholderOnFocus
									/>
								</span>
							)
						})
					}
				</div>

				{(this.state.buttonIndex != null) && (
					<form
						className="block-library-button__inline-link"
						onSubmit={(event) => {
							event.preventDefault();
							this.setState({buttonIndex: null});
						}}>
						<Dashicon icon="admin-links"/>
						<URLInput
							value={buttons[this.state.buttonIndex].url ? buttons[this.state.buttonIndex].url : '' }
							onChange={(value) => {
								const content = [...attributes.content];
								content[index].buttons[this.state.buttonIndex].url = value;
								setAttributes({content});
							}}
						/>
						<IconButton icon="editor-break" label={__('Apply')} type="submit"/>
					</form>
				)}
			</div>
		);
	}
}

const ButtonsWithFocusOutside = withFocusOutside(Buttons);

export default ButtonsWithFocusOutside;
