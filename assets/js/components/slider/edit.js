import classnames from 'classnames';
import { __ } from '@wordpress/i18n';
import { Component, Fragment } from '@wordpress/element';
import { BlockControls, RichText, MediaUpload, InspectorControls } from '@wordpress/editor';

import {
    Toolbar,
    TextControl,
    IconButton,
    withFocusOutside,
	PanelBody,
	RangeControl
} from '@wordpress/components';

import './editor.scss';

import PropTypes from 'prop-types';

import ButtonsWithFocusOutside from "./buttons";

export class SliderComponent extends Component {

    constructor() {
        super(...arguments);

        this.state = {
            selectedSlide: null,
            focusedIndex: null,
        };
    }

    handleFocusOutside() {
        this.setState({focusedIndex: false});
    }

    render() {
        const {attributes, setAttributes, isSelected} = this.props;

		const types = ["left", "right"];
		let placeholder = {};
		types.map((type) => {
			placeholder[type] = {
				text: null,
				url: null,
			};
		});

        const onSelectImage = (media) => {
            const selectedSlide = this.state.selectedSlide;
            const content = [...attributes.content];

            let value = null;

            if (!media || !media.url) {
                value = {url: null, id: null};
            } else {
                value = {url: media.url, id: media.id};
            }

            content[selectedSlide].image = value;
            setAttributes({content})
        };

		const setSpeed = ( value ) => setAttributes( { speed: value } );

        const className = 'wp-block-dr-slider';

        return (
            <Fragment>
                <BlockControls>
                    <Toolbar>
                        <MediaUpload
                            onSelect={onSelectImage}
                            type="image"
                            value={attributes.id}
                            render={({open}) => (
                                <IconButton
                                    className="components-toolbar__control"
                                    label={__('Edit background')}
                                    icon="edit"
                                    onClick={open}
                                />
                            )}
                        />
                    </Toolbar>
                </BlockControls>
				<InspectorControls>
					<PanelBody title={ __( 'Speed' ) }>
						<RangeControl
							label={ __( 'Autoplay Speed in milliseconds\n' ) }
							value={ attributes.speed }
							onChange={ setSpeed }
							min={ 500 }
							max={ 3000 }
							step={ 10 }
						/>
					</PanelBody>
				</InspectorControls>

                <div className={classnames(className, `columns-${ attributes.columns }`)}>
                    {
                        (attributes.content.length) ? (attributes.content.map((slide, i) => (
                            <div
                                key={i}
                                className={classnames(`${ className }__slide`, {
                                    'is-selected': (isSelected && this.state.selectedSlide === i),
                                })}
                                style={{
                                    backgroundImage: (attributes.content[i].image != null &&
													  attributes.content[i].image.url != null) ?
													  `url(${ attributes.content[i].image.url })` : 'none',
                                    backgroundColor: attributes.backgroundColor,
                                    color: attributes.color,
                                }}
                                onClick={() => this.setState({selectedSlide: i})}
                                onKeyDown={() => false}
                                role="textbox"
                                tabIndex="-1"
                            >
                                {(isSelected && this.state.selectedSlide === i) &&
                                <div className="block-library-gallery-item__inline-menu">
                                    <IconButton
                                        icon="no-alt"
                                        onClick={() => {
                                            const content = [...attributes.content];
                                            setAttributes({content: content.filter((el, index) => !(index === i))});
                                        }}
                                        className="blocks-gallery-item__remove"
                                        label={__('Remove Slide')}
                                    />
                                </div>
                                }

                                <TextControl
                                    type="text"
                                    placeholder="Title"
                                    className={`${ className }__title`}
                                    value={slide.title}
                                    onChange={(value) => {
                                        const content = [...attributes.content];
                                        content[i].title = value;
                                        setAttributes({content});
                                    }}
                                />

                                <RichText
                                    tagName="p"
                                    placeholder={__('Enter text...')}
                                    className={`${ className }__text`}
                                    value={slide.text}
                                    onChange={(value) => {
                                        const content = [...attributes.content];
                                        content[i].text = value;
                                        setAttributes({content});
                                    }}
                                />

								<div className={`${ className }__footer`}>
									<ButtonsWithFocusOutside setAttributes={setAttributes} attributes={attributes} index={i}/>
								</div>
                            </div>
                        ))) : null
                    }
                </div>

                {isSelected &&
                <div className="blocks-gallery-item">
                    <IconButton
                        icon="insert"
                        isDefault
                        isLarge
                        onClick={() => {
                            const content = [...attributes.content];

                            content.push({title: '', text: '', buttons: placeholder, image: {}});

                            setAttributes({content});
                        }}
                    >
                        { __('Add new')}
                    </IconButton>
                </div>
                }
            </Fragment>
        );
    }
}


SliderComponent.propTypes = {
    /**
     * The attributes for this block
     */
    attributes: PropTypes.object.isRequired,

};


export default withFocusOutside( SliderComponent );
