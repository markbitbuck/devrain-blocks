import React from "react";
import Slider from "react-slick";

import { render } from '@wordpress/element';

/**
 * Internal dependencies
 */
class SimpleSlider extends React.Component {
	render() {

		const {content, speed} = sliderSettings;

		const settings = {
			dots: true,
			infinite: true,
			autoplay: true,
			autoplaySpeed: speed,
			slidesToShow: 1,
			slidesToScroll: 1
		};

		return (
			<Slider {...settings}>
				{
					content.map((item, key) => (
						<div key={key}>
							<div style={{
								backgroundImage: (item.image !== undefined &&
									item.image.url !== undefined) ?
									`url(${ item.image.url })` : 'none',
							}}>
								<h3>{item.title}</h3>
								<p>{item.text}</p>
								<div className="buttons">
									<a href={item.buttons.left.url}>{item.buttons.left.text}</a>
									<a href={item.buttons.right.url}>{item.buttons.right.text}</a>
								</div>
							</div>
						</div>
					))
				}
			</Slider>
		);
	}
}

const containers = document.querySelectorAll(
	'.dv-block-slider'
);

containers.forEach((el) => {
	render( <SimpleSlider/>, el);
});

