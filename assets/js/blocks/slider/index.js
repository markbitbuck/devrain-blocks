/**
 * External dependencies
 */
import { __ } from '@wordpress/i18n';
import { registerBlockType } from '@wordpress/blocks';

/**
 * Internal dependencies
 */
import { SliderComponent } from '../../components/slider';

registerBlockType( 'devrain/slider', {
	title: __( 'Slider' ),
	category: 'devrain',
	description: __( 'Slider' ),
    icon: 'slides',
	keywords: [
		__( 'slider' ),
	],

    attributes: {
        content: {
            type: 'array',
            default: [],
        },
		speed: {
        	type: 'integer',
			default: 1000
		}
    },

    /**
     * Renders and manages the block.
     */
    edit( props ) {
        return <SliderComponent { ...props } />;
    },

    save( props ) {
		return null;
	},
} );
