/**
 * External dependencies
 */
import { getCategories, setCategories } from '@wordpress/blocks';
import { __ } from '@wordpress/i18n';

/**
 * Internal dependencies
 */
import '../css/editor.scss';
import '../css/style.scss';

setCategories( [
	...getCategories().filter( ( { slug } ) => slug !== 'devrain' ),
	// Add a DevRain block category
	{
		slug: 'devrain',
		title: __( 'DevRain', 'devrain-gutenberg-block' ),
	},
] );
