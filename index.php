<?php
/**
 * Plugin Name: DevRain Blocks
 * Plugin URI: https://example.com/devrain-blocks
 * Description: Devrain blocks for the Gutenberg editor.
 * Version: 0.0.0-dev
 * Author: Volodymyr Vovchok
 * Author URI: https://example.com
 * Text Domain: devrain-blocks
 *
 * @package Devrain\Blocks
 */

defined( 'ABSPATH' ) || exit;

if ( version_compare( PHP_VERSION, '5.6.0', '<' ) ) {
	return;
}

/**
 * Autoload packages.
 *
 * The package autoloader includes version information which prevents classes conflict.
 *
 * We want to fail gracefully if `composer install` has not been executed yet, so we are checking for the autoloader.
 * If the autoloader is not present, let's log the failure and display a nice admin notice.
 */
$autoloader = __DIR__ . '/vendor/autoload_packages.php';
if ( is_readable( $autoloader ) ) {
	require $autoloader;
} else {
	if ( defined( 'WP_DEBUG' ) && WP_DEBUG ) {
		error_log(  // phpcs:ignore
			sprintf(
				/* translators: 1: composer command. 2: plugin directory */
				esc_html__( 'Your installation of the Devrain Blocks feature plugin is incomplete. Please run %1$s within the %2$s directory.', 'devrain-blocks' ),
				'`composer install`',
				'`' . esc_html( str_replace( ABSPATH, '', __DIR__ ) ) . '`'
			)
		);
	}
	/**
	 * Outputs an admin notice if composer install has not been ran.
	 */
	add_action(
		'admin_notices',
		function() {
			?>
			<div class="notice notice-error">
				<p>
					<?php
					printf(
						/* translators: 1: composer command. 2: plugin directory */
						esc_html__( 'Your installation of the Devrain Blocks feature plugin is incomplete. Please run %1$s within the %2$s directory.', 'devrain-blocks' ),
						'<code>composer install</code>',
						'<code>' . esc_html( str_replace( ABSPATH, '', __DIR__ ) ) . '</code>'
					);
					?>
				</p>
			</div>
			<?php
		}
	);
	return;
}

add_action( 'plugins_loaded', [ \DevRain\Blocks\Plugin::class, 'init' ] );
