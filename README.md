# DevRain Blocks

Task plugin for Gutenberg. 

#### Based on plugins:

- [WooCommerce Gutenberg Products](https://github.com/woocommerce/woocommerce-gutenberg-products-block)
- [Stag Blocks](https://wordpress.org/plugins/stag-blocks/) (Stats block)

## Installing the development version

1. Make sure you have WordPress 5.0+
2. Get a copy of this plugin using the green "Clone or download" button on the right.
3. `npm install` to install the dependencies.
4. `composer install` to install core dependencies.
5. `npm run build` (build once) or `npm start` (keep watching for changes) to compile the code.
6. Activate the plugin.

The source code is in the `assets/` folder and the compiled code is built into `build/`.
